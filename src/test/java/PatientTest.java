import org.junit.jupiter.api.Test;

import javax.print.Doc;

import static org.junit.jupiter.api.Assertions.*;

class PatientTest {

    @Test
    void setDiagnosis() {
        Patient testPatient = new Patient("firstname", "lastName", "1");
        GeneralPractitioner testDoctor = new GeneralPractitioner("name", "lastName", "2");
        testDoctor.setDiagnosis(testPatient,"Sick");
        assertEquals("Sick",testPatient.getDiagnosis());
    }
}
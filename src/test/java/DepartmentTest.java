import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {

    @Test
    @DisplayName("This is a test for a successful remove method application")
    void positiveRemoveTest() {
        try {
            Department testDepartment = new Department("department");
            Employee testEmployee = new Employee("first name", "Last name", "1");
            testDepartment.addEmployee(testEmployee);
            testDepartment.remove(testEmployee);
        }catch (RemoveException e){
            System.err.println(e);
        }
    }

    @Test
    @DisplayName("This is a a test for an unsuccessful remove method application")
    void negativeRemoveTest(){
        try {
            Department testDepartment = new Department("department");
            Employee testEmployee = new Employee("first name", "Last name", "1");
            Employee notInDepartemntEmployee = new Employee("name", "last name", "2");
            testDepartment.addEmployee(testEmployee);
            testDepartment.remove(notInDepartemntEmployee);
        }catch (RemoveException e){
            System.err.println(e);
        }

    }
}
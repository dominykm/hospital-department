/**
 * Patient class that is a subclass to Person
 * it describes a patient with a diagnosis
 */
public class Patient extends Person implements Diagnosable{

    private String diagnosis = "";

    protected Patient(String firstName, String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);

    }


    protected String getDiagnosis(){
        return diagnosis;
    }

    @Override
    public String toString(){
        return getFirstName() + " " + getLastName() + ": " + getSocialSecurityNumber() +", diagnosis: " + diagnosis;
    }

    @Override
    public void setDiagnosis(String newDiagnosis) {
        diagnosis = newDiagnosis;
    }
}

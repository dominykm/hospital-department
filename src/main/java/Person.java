/**
 * Class to represent a person with first name, second name and social security number
 * this class has only set and get methods for the 3 variables
 */
public abstract class Person {

    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    public Person(String firstName, String lastName, String socialSecurityNumber){
        this.firstName=firstName;
        this.lastName=lastName;
        this.socialSecurityNumber=socialSecurityNumber;
    }

    public String getSocialSecurityNumber(){
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String newSocialSecurityNumber){
        socialSecurityNumber = newSocialSecurityNumber;
    }

    public void setFirstName(String newFirstName){
        firstName = newFirstName;
    }

    public void setLastName(String newLastName){
        lastName = newLastName;
    }

    public String getFirstName(){
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public String getFullName(){
        return firstName + " " + lastName;
    }

    public String toString(){
        return firstName + " " + lastName + ": " + socialSecurityNumber;
    }


}

/**
 * Subclass for Doctor class and this class just states what kind of doctor this person is
 */
public class Surgeon extends Doctor{

    public Surgeon(String firstName, String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);
    }

    /**
     * method to set a diagnosis for a specific patient
     * @param patient patient that needs a new diagnosis
     * @param diagnosis the new diagnosis for the patient
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis){
        patient.setDiagnosis(diagnosis);
    }
}

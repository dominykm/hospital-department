/**
 * subclass for Person class, this class describes an employee
 */
public class Employee extends Person{

    public Employee(String firstName, String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);
    }

    @Override
    public String toString(){
        return getFirstName() + " " + getLastName();
    }
}

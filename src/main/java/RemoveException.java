/**
 * Exception class that is thrown when the remove method is called and the person does not exist
 */
public class RemoveException extends Exception{
    final static long serialVersionUID= 1L;

    public RemoveException(String message){
        super("The person with this social security number: " + message + " does not exist in this department");
    }

}

/**
 * A class that describes an department
 * Department has to list, one with all of employees and one with all of the patients
 *
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Department {
    private String departmentName;
    private HashMap<String,Employee> employees = new HashMap<>();
    private HashMap<String,Patient> patients = new HashMap<>();

    public Department(String departmentName){
        this.departmentName=departmentName;
    }

    public void setDepartmentName(String newDepartmentName){
        departmentName=newDepartmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public HashMap<String,Employee> getEmployees(){
        return employees;
    }

    /**
     * method to add a new employee to the employee list
     * @param newEmployee the new employee to be added
     */
    public void addEmployee(Employee newEmployee){
    if(!employees.containsKey(newEmployee.getSocialSecurityNumber())){
        employees.put(newEmployee.getSocialSecurityNumber(), newEmployee);
        }
    }

    public HashMap<String, Patient> getPatients() {
        return patients;
    }

    /**
     * method to add a new patient to the patient list
     * @param newPatient the new patient to be added
     */
    public void addPatient(Patient newPatient){
        if(!patients.containsKey(newPatient.getSocialSecurityNumber())){
            patients.put(newPatient.getSocialSecurityNumber(), newPatient);
        }

    }

    public void remove(Person person) throws RemoveException{
        if(employees.containsKey(person.getSocialSecurityNumber())){
            employees.remove(person);
            System.out.println("Person was removed successfully");
        }else{
            throw new RemoveException(person.getSocialSecurityNumber());
        }

    }

    @Override
    public String toString(){
        String listEmployees = "List over all employees: ";
        for(Map.Entry<String,Employee> employee: employees.entrySet()){
            listEmployees += employee.toString() + ", ";
        }
        String listPatients = "List over all Patients: ";

        for(Map.Entry<String,Patient> patient: patients.entrySet()){
            listEmployees += patient.toString() + ", ";
        }
        return listEmployees + getEmployees() + "\n" + listPatients + getPatients() +"\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }
}

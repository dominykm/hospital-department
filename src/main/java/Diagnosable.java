/**
 * interface class to set a new diagnosis
 */
public interface Diagnosable {

    void setDiagnosis(String newDiagnosis);
}

/**
 * simple client to test the client
 */

import java.util.Map;

public class HospitalClient {


    public static void main(String[] args) {
        try{
            Hospital hospital = new Hospital("Oslo hospital");
            HospitalTestData.fillRegisterWithTestData(hospital);
            System.out.println(hospital);

            //test to remove the same person from both departments, removing from the first department succeeds,
            //while removing from the second department fails, because the person is not there.
            for(Map.Entry<String, Department> department : hospital.getDepartments().entrySet()){
                department.getValue().remove(new Employee("Odd Even", "Primtallet", "1"));
            }



        }catch (RemoveException e){
            System.out.println(e);
        }

    }
}

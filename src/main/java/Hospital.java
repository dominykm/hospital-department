/**
 * Class for a hospital
 * a hospital has multiple departments in it and a hospital name
 */

import java.util.HashMap;
public class Hospital {

    private String hospitalName;
    private HashMap<String, Department> departments = new HashMap<>();

    public Hospital(String hospitalName){
        this.hospitalName=hospitalName;

    }

    public String getHospitalName() {
        return hospitalName;
    }

    public HashMap<String, Department> getDepartments() {
        return departments;
    }

    /**
     * method to add a new department, if one with the same name does not exist from before
     * @param newDepartment the new department to be added
     */
    public void addDepartments(Department newDepartment){
        if(!departments.containsKey(newDepartment.getDepartmentName())){
            departments.put(newDepartment.getDepartmentName(),newDepartment);
        }
    }

    @Override
    public String toString(){
        return "List over all departments: " + getDepartments();
    }
}
